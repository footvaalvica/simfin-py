# simfin

# NOTE: This project is not maintained anymore. I recommend you use https://bitbucket.org/footvaalvica/simfin instead. 

[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](http://github.com/syl20bnr/spacemacs)

A simple money managing application, if you think other apps have too many features, this one's for you.

Simfin works like this, ```add``` to add money, ```remove```to remove it, ```howmuch``` to check how much you have and ```history``` to check the latest change you made.

You need python 3, to install it go to https://www.python.org/downloads/

To execute it in Fedora 23, extract the simfin-master.zip to somewhere on your computer, navigate to simfin-master and then inside that directory do ```python3 simfin.py```
